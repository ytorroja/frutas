
import toxi.geom.*;
import toxi.geom.mesh2d.*;

import toxi.util.*;
import toxi.util.datatypes.*;

import toxi.processing.*;
    
class Naranja {

  Voronoi voronoi;

  // helper class for rendering

  
  // optional polygon clipper
  PolygonClipper2D clip;  
     
  float points[][];
  
  int   gajos;
  float radio;
  float grano;
  float simetria;
  
  Naranja(int gajos, float radio, float grano, float simetria, int steps) {
    this.gajos    = gajos;
    this.radio    = radio;
    this.grano    = grano;
    this.simetria = simetria;
    points        = new float[4][32 * gajos];  
    voronoi       = new Voronoi();
    clip = new SutherlandHodgemanClipper(new Rect(width*0.125, height*0.125, width*0.75, height*0.75));
     
  }

  Naranja(int gajos, float radio) {
    this(gajos, radio, 0.01, 0.5, 200);
  }

  Naranja() {
    this(8, 100, 0.11, 0.5, 200);
  }
  
  void setGajos(int g) {
    gajos  = g;
    points = new float[4][32 * gajos]; 
    generate();
  }

  void setRadio(float r) {
    radio  = r;
    generate();
  }

  void setGrano(float g) {
    grano  = g;
    generate();
  }

  void setSimetria(float s) {
    simetria  = s;
    generate();
  }
  
  void generate(long seed) {
    noiseSeed(seed);
    generate();
  }
  
  void generate() { 
    float offset = 0;
    voronoi = new Voronoi();
    //voronoi.addPoint(new Vec2D(0, 0));
    float radInc   = radio / 4;    
    for(int i = 1; i < 4; i++) {
      int capaGajos = gajos * i * i;

      for(int j = 0; j < capaGajos; j++) {
        points[i][j] = (noise(offset) * (1-simetria)  + simetria)  * radInc + points[i-1][(int)(j/i/i)];
        offset += grano;
      }
      radInc += radio / 2;
      float diffInc = (points[i][capaGajos - 1] - points[i][0]) / capaGajos;
      for(int j = 0; j < capaGajos; j++) {
        points[i][j] -= diffInc * j;
      }  
      
      float angInc   = TWO_PI / capaGajos;
      float ang = 0;    
      for(int j = 0; j < capaGajos; j++) {  
        float x = points[i][j] * cos(ang);
        float y = points[i][j] * sin(ang);
        voronoi.addPoint(new Vec2D(x, y));
        ang    += angInc;
      }
      radInc/=8;
    }
  }
  
  void draw(float x, float y) {
    pushStyle();
    pushMatrix();
    translate(x, y);
    stroke(0);
    noFill();
    // draw all voronoi polygons, clip them if needed...
    boolean doClip = false;
    for (Polygon2D poly : voronoi.getRegions()) {
      for(int i = 0; i < 30; i++) poly.smooth(0.01,0.05);
      if (doClip) {
        gfx.polygon2D(clip.clipPolygon(poly));
      } 
      else {

        gfx.polygon2D(poly);
      }
    }
    // draw delaunay triangulation
    boolean doShowDelaunay = false;
    if (doShowDelaunay) {
      stroke(0, 0, 255, 50);
      beginShape(TRIANGLES);
      for (Triangle2D t : voronoi.getTriangles()) {
        gfx.triangle(t, false);
      }
      endShape();
    }
    noStroke();
    boolean doShowPoints = false;
    if (doShowPoints) {
      fill(255, 0, 255);
      noStroke();
      for (Vec2D c : voronoi.getSites()) {
        ellipse(c.x, c.y, 5, 5);
      }
    }
    popMatrix();
    popStyle();
 }
 
  
}
