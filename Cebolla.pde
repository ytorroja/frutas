class Cebolla {
  
  int   steps;
  float points[][];
  
  int   capas;
  float radio;
  float grano;
  float simetria;
  
  
  Cebolla(int capas, float radio, float grano, float simetria, int steps) {
    this.capas    = capas;
    this.radio    = radio;
    this.grano    = grano;
    this.simetria = simetria;
    this.steps    = steps;
    points        = new float[capas][steps];  
  }

  Cebolla(int capas, float radio) {
    this(capas, radio, 0.01, 0.5, 300);
  }

  Cebolla() {
    this(6, 54, 0.01, 0.5, 300);
  }
  
  void setCapas(int c) {
    capas  = c;
    points = new float[capas][steps]; 
    generate();
  }

  void setRadio(float r) {
    radio  = r;
    generate();
  }

  void setGrano(float g) {
    grano  = g;
    generate();
  }

  void setSimetria(float s) {
    simetria  = s;
    generate();
  }
  
  void generate(long seed) {
    noiseSeed(seed);
    generate();
  }
  
  void generate() { 
    float offset = 0;
    float angInc   = TWO_PI / steps;
    float radInc   = radio / capas ;
    float max = 0;

    for(int i = 1; i < capas; i++) {      
      for(int j = 0; j < steps; j++) {
        points[i][j] = (noise(offset) * (1-simetria)  + simetria)  * radInc + points[i-1][j];
        if (abs(points[i][j]) > max) max = abs(points[i][j]);
        offset += grano;
      }
      
      float diffInc = (points[i][steps - 1] - points[i][0]) / steps;
      for(int j = 0; j < steps; j++) {
        points[i][j] -= diffInc * j;
      }   
    }    
    for(int i = 1; i < capas; i++) {      
      for(int j = 0; j < steps; j++) {
        points[i][j] /= max;
        points[i][j] *= radio;
      }
    }
        
  }
  
  void draw(float x, float y) {
    pushMatrix();
    translate(x, y);
    float angInc   = TWO_PI / steps;
    for(int i = 1; i < capas; i++) {
      pushMatrix();
      float x1, x2, y1, y2;
      float ang = 0;
      x1 = points[i][0];
      y1 = 0;
      for(int j = 1; j < steps; j++) {
        x2 = points[i][j] * cos(ang);
        y2 = points[i][j] * sin(ang);
        ang += angInc;
        line(x1, y1, x2, y2);
        x1 = x2;
        y1 = y2;
        // rotate( angInc );
        // point( 0, points[i][j] );
      }
      x2 = points[i][0];
      y2 = 0;
      line(x1, y1, x2, y2);
      popMatrix();
    }
    popMatrix();
  }
  
}
