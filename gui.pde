Slider radio;
Slider capas;
Range grano;
Range simetria;

void setupGui() {
  float x = 10;
  float y = 10;
  float sliderHeight = 20;
  float sliderWidth  = 200;
  
  CColor c = new CColor()
    .setForeground( color(150) )
    .setBackground( color(200) )
    .setValueLabel( color(0) )
    .setCaptionLabel( color(0) )
    .setActive( color(100) )
    ;

  radio = cp5.addSlider("radio")
     .setPosition(x, y)
     .setRange(0,255)
     .setValue(40)
     .setDecimalPrecision(0)
     .setColor( c )
     .addCallback(
       new CallbackListener() {
         public void controlEvent(CallbackEvent theEvent) {       
           cebolla.setRadio( radio.getValue() );
           //naranja.setRadio( radio.getValue() );
         }
       }
     )     
     ;

  // y += sliderHeight;
  x += sliderWidth;
  capas = cp5.addSlider("capas")
     .setPosition(x, y)
     .setRange(2,20)
     .setValue(6)
     .setDecimalPrecision(0)
     .setColor( c )     
     .setNumberOfTickMarks(10)
     .addCallback(
       new CallbackListener() {
         public void controlEvent(CallbackEvent theEvent) {       
           cebolla.setCapas( (int)capas.getValue() );
           //naranja.setGajos( (int)capas.getValue() ); 
         }           }
     )     
     ;
     
  // y += sliderHeight;
  x += sliderWidth;
  grano = cp5.addRange("grano")
     .setPosition(x, y)
     .setRange(0, 1)
     .setValue(0.5)
     .setDecimalPrecision(1)
     .setColor( c )     
     .addCallback(
       new CallbackListener() {
         public void controlEvent(CallbackEvent theEvent) {       
           cebolla.setGrano( grano.getValue()/100 );
           //naranja.setGrano( grano.getValue()/10 );
         }
       }
     )     
     ;
     
  // y += sliderHeight;
  x += sliderWidth;
  simetria = cp5.addRange("simetria")
     .setPosition(x, y)
     .setRange(-5, 5)
     .setValue(0.5)
     .setDecimalPrecision(2)
     .setColor( c )     
     .addCallback(
       new CallbackListener() {
         public void controlEvent(CallbackEvent theEvent) {       
           cebolla.setSimetria( simetria.getValue() );
           //naranja.setSimetria( simetria.getValue() );
         }
       }
     )     
     ;
     

}
