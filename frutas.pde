import controlP5.*;
import processing.pdf.*;

ControlP5 cp5;

boolean bDraw = true;
boolean bSave = false;

Cebolla cebolla;
Naranja naranja;

float rad;

ToxiclibsSupport gfx;

void setup() {
  size(800, 600);
  stroke(0);
  smooth();
  cp5 = new ControlP5(this);
  
  setupGui();
 
  gfx = new ToxiclibsSupport(this);
 
  cebolla = new Cebolla();
  cebolla.generate();
  
  naranja = new Naranja();
  naranja.generate();
  
  noiseDetail(3, 0.4); 
}


void draw() {
  if (!bDraw) return;
  background(255);
 
  if (bSave) beginRecord(PDF, "frame-####.pdf");
  
  float x = 0; float y = 30;
  float g = 0; float s = 0;
  
  float inc_g = (grano.getHighValue() - grano.getLowValue()) / 3; 
  for(g = grano.getLowValue(), y += 1.1 * cebolla.radio; g < grano.getHighValue() + inc_g/2; g += inc_g, y += 2.1 * cebolla.radio) {
    cebolla.setGrano( g );

    float inc_s = (simetria.getHighValue() - simetria.getLowValue()) / 6; 
    for(s = simetria.getLowValue(), x = 1.1 * cebolla.radio; s < simetria.getHighValue() + inc_s/2; s += inc_s, x += 2.1 * cebolla.radio) {
      cebolla.setSimetria( s );
      cebolla.draw(x, y);
    }
  }
  // cebolla.draw(width/2, height/2);
  // naranja.draw(width/2, height/2);
  
  if (bSave) {
    endRecord();
    bSave = false;
  }
  
}

void keyPressed() {
  if (key == 'n') {
    cebolla.generate((long)random(0, 0xfffffff));
    naranja.generate((long)random(0, 0xfffffff));
  }
  if (key == ' ') bDraw = !bDraw;
  if (key == 's') bSave = true;
}
